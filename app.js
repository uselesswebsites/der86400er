function analogSecondsClock() {
    let now = new Date();

    let hoursInSeconds = Math.floor(now.getHours()*60*60)
    let minutesInSeconds = Math.floor(now.getMinutes()*60)
    let seconds = Math.floor(now.getSeconds())
    let secondsPassed = hoursInSeconds + minutesInSeconds + seconds;
    let deg = Math.floor( 360 / (86400 / secondsPassed) )

    document.getElementById("secondsLine").setAttribute('title', deg)
    document.getElementById("secondsLine").setAttribute('transform', 'rotate('+ deg +' 500 500)')
}

analogSecondsClock();
window.addEventListener("load", function() {
    timer = setInterval(analogSecondsClock, 1000);
});
